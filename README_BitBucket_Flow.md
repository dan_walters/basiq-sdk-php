## How to Tag a Release on BitBucket:
- Pull the latest version of master
- Identify the commit hash of the version of the code which will be the cutoff point / the last commit to be pushed to production
- Identify the release version. ex. 1.3
- On Git Bash, add the following commands:
  ```bash
  #git tag -a <release version> <commit hash>
  git tag -a 1.3.0 571fb31d
  ```
- Push the release version to BitBucket:
  ```bash
  #git push origin <release version>
  git push origin 1.3.0
  ```