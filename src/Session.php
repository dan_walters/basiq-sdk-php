<?php

namespace Basiq;

use GuzzleHttp\Client;
use Basiq\Utilities\ResponseParser;
use Basiq\Services\UserService;

class Session
{
    private $apiKey;

    private $accessToken;

    public $apiClient;

    private $sessionTimestamp;

    private $tokenValidity;

    private $apiVersion;

    public function __construct($apiKey, $apiVersion = '1.0', $scope = 'SERVER_ACCESS')
    {
        $this->apiClient = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://au-api.basiq.io',
            // You can set any number of default request options.
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            'timeout' => 30.0,
            'http_errors' => false
        ]);

        $this->tokenValidity    = 3600;
        $this->sessionTimestamp = 0;
        $this->apiKey           = $apiKey;
        $this->apiVersion       = $apiVersion;
        $this->accessToken      = $this->getAccessToken($scope);
    }

    public function getApiVersion()
    {
        return $this->apiVersion;
    }
 
    public function getAccessToken($scope = 'SERVER_ACCESS')
    {
        // TODO: Ask about it! https://api.basiq.io/v2.1/reference/authentication
//        if (!in_array($scope, ['SERVER_ACCESS', 'CLIENT_ACCESS'])) {
//            throw new \Exception('Expected to be either (SERVER_ACCESS, CLIENT_ACCESS)');
//        }

        if (time() - $this->sessionTimestamp < $this->tokenValidity) {
            return $this->accessToken;
        }

        if (!\in_array($this->apiVersion, ['2.0', '1.0'], true)) {
            error_log("Given version isn't supported");
        }

        $response = $this->apiClient->post(
            '/token',
            [
                'headers' => [
                    'Content-type'  => 'application/json',
                    'Authorization' => 'Basic ' . $this->apiKey,
                    'basiq-version' => $this->apiVersion
                ],
                // TODO: Ask about it! https://api.basiq.io/v2.1/reference/authentication
//                'data' => ['scope' => $scope]
            ]
        );

        // ADD LOGIC TO CHECK FOR VALID RESPONSE
//        if ($response->getStatusCode() !== 200) {
//            throw new \Exception('Token not set.');
//        }

        $this->sessionTimestamp = time();

        $body = ResponseParser::parse($response);
        $this->tokenValidity = $body['expires_in'];

        return $body['access_token'];
    }

    public function getInstitutions()
    {
        $response = $this->apiClient->get(
            '/institutions',
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->getAccessToken()
                ]
            ]
        );

        return ResponseParser::parse($response);
    }

    public function getInstitution($id)
    {
        $response = $this->apiClient->get(
            '/institutions/' . $id,
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->getAccessToken()
                ]
            ]
        );

        return ResponseParser::parse($response);
    }

    public function getUser($id)
    {
        return (new UserService($this))->get($id);
    }

    public function forUser($id)
    {
        return (new UserService($this))->forUser($id);
    }
}
