<?php

namespace Basiq\Services;

use Basiq\Session;

class Service
{
    public $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }
}