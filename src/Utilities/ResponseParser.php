<?php

namespace Basiq\Utilities;

use Exception;
use JsonException;
use Basiq\Exceptions\HttpResponseException;
use Psr\Http\Message\ResponseInterface;

class ResponseParser
{
    public static function parse(ResponseInterface $response)
    {
        $body = $response->getBody();

        if ($body->getSize() > 0) {
            $contents = $body->__toString();
            try {
                $body = json_decode($contents, true, 512, JSON_THROW_ON_ERROR);
            } catch (JsonException $e) {
                return null;
            }

            if ($body === null) {
                throw new Exception('Invalid response received from server. Check log for the response');
            }

            if ($response->getStatusCode() > 299) {
                throw new HttpResponseException($body, $response->getStatusCode());
            }

            return $body;
        }

        return null;
    }
}