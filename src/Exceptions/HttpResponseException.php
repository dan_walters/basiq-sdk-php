<?php

namespace Basiq\Exceptions;

use Exception;

class HttpResponseException extends Exception
{
    public $response;
    public $statusCode;
    public $message;

    public function __construct($body, $statusCode)
    {
        if (isset($body['data'])) {
            $message = trim(array_reduce(
                $body['data'],
                static function ($sum, $error) {
                    return $sum . $error['detail'];
                }, ''));
        } else {
            $message = 'Unexpected error from server';
        }

        $this->response   = $body;
        $this->statusCode = $statusCode;
        $this->message    = $message;
    }

    public function getResponse()
    {
        return $this->response;
    }
}